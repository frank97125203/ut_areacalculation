﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaCalculation
{
    public class Circle : IShape
    {
        private readonly double _radius;
        const double pi = Math.PI;
        public Circle(double radius)
        {
            _radius = radius;
        }

        public double GetArea()
        {
            return _radius * _radius * pi;
        }
    }
}
