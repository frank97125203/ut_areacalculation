﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaCalculation
{
    public class Square : IShape
    {
        private readonly double _side;
        public Square(double side)
        {
            _side = side;
        }

        public double GetArea()
        {
            return _side * _side;
        }
    }
}
