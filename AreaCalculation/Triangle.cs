﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaCalculation
{
    public class Triangle : IShape
    {
        private readonly double _triangleBase;
        private readonly double _triangleHeight;
        public Triangle(double triangleBase, double triangleHeight)
        {
            _triangleBase = triangleBase;
            _triangleHeight = triangleHeight;
        }

        public double GetArea()
        {
            return _triangleBase * _triangleHeight / 2;
        }
    }
}
