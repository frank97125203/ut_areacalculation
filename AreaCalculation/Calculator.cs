﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaCalculation
{
    public class Calculator
    {
        public static double GetTotalArea(params IShape[] areas)
        {
            return Math.Round(areas.Sum(a => a.GetArea()), 2);
        }
    }
}
