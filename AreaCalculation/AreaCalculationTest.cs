using NUnit.Framework;

namespace AreaCalculation
{
    public class AreaCalculationTest
    {
        [Test]
        public void AreaOfATriangleIsHalfOfItsBaseMultipliedByHeight()
        {
            const double triangleBase = 6;
            const double triangleHeight = 4;

            var triangle = new Triangle(triangleBase, triangleHeight);

            Assert.AreEqual(12, Calculator.GetTotalArea(triangle));
        }

        [Test]
        public void AreaOfASquareIsSquareOfSide()
        {
            const double side = 6;

            var square = new Square(side);

            Assert.AreEqual(36, Calculator.GetTotalArea(square));
        }

        [Test]
        public void AreaOfARectangleIsWidthMultipliedByHeight()
        {
            const double height = 4;
            const double width = 8;

            var rectangle = new Rectangle(height, width);

            Assert.AreEqual(32, Calculator.GetTotalArea(rectangle));
        }

        [Test]
        public void AreaOfACircleIsSquareOfRadiusMulitpliedByPi()
        {
            const double radius = 3;

            var circle = new Circle(radius);

            Assert.AreEqual(28.27, Calculator.GetTotalArea(circle));
        }

        [Test]
        public void TotalAreaIsSumOfAreasOfDifferentShapes()
        {
            Assert.AreEqual(49.14, Calculator.GetTotalArea(new Rectangle(4, 2), new Rectangle(3, 4), new Circle(1), new Square(1), new Triangle(10, 5)));
        }

        [Test]
        public void TotalAreaIsRoundedTo2Decimals()
        {
            Assert.AreEqual(4.45, Calculator.GetTotalArea(new Rectangle(1.112, 2), new Rectangle(1.111, 2)));
        }

        [Test]
        public void TotalAreaIs0WhenThereAreNoShapes()
        {
            Assert.AreEqual(0, Calculator.GetTotalArea());
        }
    }
}